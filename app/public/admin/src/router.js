import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Dashboard from './views/Dashboard.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ './views/Login.vue')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
    },
    {
      path: '*',
      name: 'fallback',
      component: Home
    },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.fullPath === '/dashboard') {
    if (!router.app.$getStore('token')) {
      next('/login');
      return
    }
  }
  if (to.fullPath === '/login') {
    if (router.app.$getStore('token')) {
      next('/dashboard');
      return
    }
  }
  next();
});
export default router