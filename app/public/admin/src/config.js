const config = {
  'development': {
    api: 'http://localhost:3859/campaign/luckdraw2019/api',
  },
  'production': {
    // api: 'http://localhost:3859/campaign/luckdraw2019/api',
    api: 'https://campaign.hantec-au.com/campaign/luckdraw2019/api',
  },
};

export default config[process.env.NODE_ENV] || config['development']