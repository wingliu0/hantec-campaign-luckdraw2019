import _ from 'lodash';
import axios from "axios";
import config from "@/config";

const store = {}

export default {

  init(Vue) {
    // set user_id
    Vue.prototype.$setStore('name', localStorage.getItem('name'));
    Vue.prototype.$setStore('token', localStorage.getItem('token'));
    if (!Vue.prototype.$getStore('user_id')) {
      // Vue.prototype.$setStore('user_id', Vue.prototype.$guidGenerator())
      // localStorage.setItem('user_id', Vue.prototype.$getStore('user_id'))
    }
  },

  // The install method will be called with the Vue constructor as
  // the first argument, along with possible options
  install(Vue, options) {
    Vue.prototype.$store = () => store

    Vue.prototype.$logout = async () => {
      Vue.prototype.$setStore('token', '');
      localStorage.setItem('name', '')
      Vue.prototype.$setStore('name', '');
      localStorage.setItem('token', '')

    }
    Vue.prototype.$login = async (name, password) => {
      return axios.post(config.api + '/login', {
        name,
        password,
      }).then(res => res.data)
        .then(({ token, name }) => {
          Vue.prototype.$setStore('token', token);
          localStorage.setItem('token', Vue.prototype.$getStore('token'))
          Vue.prototype.$setStore('name', name);
          localStorage.setItem('name', Vue.prototype.$getStore('name'))
          return 0
        })
        .catch(e => {
          return 1
        })
    }
    // Vue.prototype.$login('a', 'a')

    Vue.prototype.$getCoupon = async () => {
      return axios.get(config.api + '/admin/coupon', {
        headers: {
          'x-token': Vue.prototype.$getStore('token')
        }
      }).then(r => r.data.coupons)
    }
    Vue.prototype.$editCoupon = async () => {
    }
    Vue.prototype.$addCoupon = async (number, clientCode) => {
      return axios.post(config.api + '/admin/coupon', {
        number,
        clientCode,
      }, {
        headers: {
          'x-token': Vue.prototype.$getStore('token')
        }
      }).then(r => r.data.coupons)
        .catch(e => {
          if (_.get(e, 'response.data')) {
            alert(_.get(e, 'response.data'))
          }
          throw e;
        })
    }
    Vue.prototype.$setStore = (key, value) => {
      _.set(store, key, value)
    }
    Vue.prototype.$getStore = (key = null, fallback = null) => {
      if (!key) {
        return store
      }
      return _.get(store, key, fallback);
    }

    Vue.prototype.$deleteCoupon = async ({ id }) => {
      return axios.delete(config.api + '/admin/coupon', {
        headers: {
          'x-token': Vue.prototype.$getStore('token'),
          'x-id': id,
        }
      }).then(r => r.data.coupons)
    }


    this.init(Vue);
  }
}