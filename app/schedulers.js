const schedule = require('node-schedule')
const moment = require('moment')
const api = require('./api')
const start = () => {
  console.log('htcampaign scheduler started!');
  api.getDeposit({ busDtStr: moment().format('YYYY-MM-DD') })
  schedule.scheduleJob('0 * * * *', () => {
    api.getDeposit({ busDtStr: moment().format('YYYY-MM-DD') })
  })
}

module.exports = {
  start,
}