const uuidv4 = require('uuid/v4');
const db = require('./db/models');
const _ = require('lodash');
const axios = require('axios');
const async = require('async');
const querystring = require('querystring');
const { randomNumber } = require('./libs/utility')
const Sequelize = require('sequelize');
const moment = require('moment')

const MAX_COUPON_PER_USER = 10

const generateCoupons = ({
  coCode,
  dt,
  cltCode,
  aeCode,
  ccy,
  amt,
}) => {
  const couponExchangeMapping = {
    2000: 1,
    6000: 4,
    8000: 6,
    10000: 8,
    13000: 10,
  }
  const level = _.max(
    Object.keys(couponExchangeMapping)
      .map(o => Number(o))
      .filter(o => amt > o)
  )
  const numberOfCoupons = couponExchangeMapping[level]
  if (!numberOfCoupons) { return [] }
  const compareDate = moment(dt, 'YYYY/MM/DD HH:mm:ss')
  const startDate = moment('2019-09-23')
  const finishDate = moment('2019-10-18')
  if (!compareDate.isBetween(startDate, finishDate, 'days', '[]')) { console.log('deposit not between campaign dates'); return [] }
  const depositId = [aeCode, amt, ccy, cltCode, coCode, dt].join('|')
  return Array.from(new Array(numberOfCoupons), (v, i) => {
    const serialNumber = `${i + 1}/${numberOfCoupons}`
    const number = randomNumber(6)
    return {
      id: `${depositId}|${serialNumber}`,
      depositId,
      serialNumber,
      cltCode,
      fromDeposit: true,
      number,
    }
  })
}
const depositsToCoupons = deposits => {
  return deposits.reduce((acc, deposit) => {
    acc = acc.concat(generateCoupons(deposit))
    return acc
  }, [])
}

const hantec = {
  getClient: async ({
    apiLogin,
    apiPassword,
    mode,
    cltCode,
    fromDateStr,
    toDateStr,
    Submit,
  }) => {
    return axios({
      method: 'post',
      url: 'http://203.80.250.6:38080/redeem/remoting/ClientLoginService',
      data: querystring.stringify({
        apiLogin: "mtUATU001",
        apiPassword: "ApiUAT1122",
        mode: "1",
        cltCode,
        fromDateStr: "",
        toDateStr: "",
        Submit: "Submit"
      }),
    }).then(res => {
      if (_.get(res, 'data.status') === 'success') {
        return _.get(res, 'data')
      }
      throw 'error'
    })
  },
  checkLogin: async ({
    apiLogin,
    apiPassword,
    mode,
    type,
    cltCode,
    loginPwd,
    Submit,
  }) => {
    return axios({
      method: 'post',
      url: 'http://203.80.250.6:38080/redeem/remoting/ClientLoginService',
      data: querystring.stringify({
        apiLogin: "mtUATU001",
        apiPassword: "ApiUAT1122",
        mode: "0",
        type: "M",
        cltCode: '52042',
        loginPwd: 'A123456',
        Submit: "Submit",
      }),
    }).then(res => {
      if (_.get(res, 'data.isLoginSuccess') === true && _.get(res, 'data.status') === 'success') {
        return _.get(res, 'data')
      }
      throw 'error'
    })
  },
  getDeposit: async ({
    apiLogin,
    apiPassword,
    mode,
    busDtStr,
    Submit,
    cltCode,
  }) => {
    return axios({
      method: 'post',
      url: 'http://203.80.250.6:38080/redeem/remoting/OrderService',
      data: querystring.stringify({
        apiLogin: "mtUATU001",
        apiPassword: "ApiUAT1122",
        mode: "3",
        busDtStr,
        Submit: "Submit",
      }),
    }).then(res => {
      if (_.get(res, 'data.status') === 'success') {
        return _.get(res, 'data')
      }
      throw 'error'
    })
  },
  getLiqOrder: async ({
    apiLogin,
    apiPassword,
    mode,
    busDtStr,
    Submit,
    cltCode,
  }) => {
    return axios({
      method: 'post',
      url: 'http://203.80.250.6:38080/redeem/remoting/OrderService',
      data: querystring.stringify({
        apiLogin: 'mtUATU001' || "mtHnzU001",
        apiPassword: 'ApiUAT1122' || "ApiH123456",
        mode: "2",
        busDtStr: "2019-05-07",
        Submit: "Submit",
      }),
    }).then(res => {
      if (_.get(res, 'data.status') === 'success') {
        return _.get(res, 'data')
      }
      throw 'error'
    })
  },
}

// hantec.getLiqOrder({})
// hantec.getLiqOrder({}).then(r => {
//   console.log(r);
// })



module.exports = {
  checkLogin: async ({
    apiLogin,
    apiPassword,
    mode,
    type,
    cltCode,
    loginPwd,
    fromDateStr,
    toDateStr,
    Submit,
  }) => {
    const { isLoginSuccess } = await hantec.checkLogin({
      apiLogin,
      apiPassword,
      mode,
      type,
      cltCode,
      loginPwd,
      Submit,
    })
    if (isLoginSuccess !== true) { throw 'login fail' }
    const client = await db.Client.findOne({ where: { cltCode } })

    if (!client) {
      const { clientDataVos, status } = await hantec.getClient({
        apiLogin,
        apiPassword,
        mode,
        cltCode,
        fromDateStr,
        toDateStr,
        Submit,
      })
      if (status !== 'success') { throw 'client !exist' }
      const name = _.get(clientDataVos, '0.name')
      const token = uuidv4().replace(/-/g, '')
      db.Client.create({
        cltCode,
        token,
        name,
      })
      return { token }
    } else {
      return { token: client.token }
    }
  },
  getCoupons: async ({
    apiLogin,
    apiPassword,
    mode,
    busDtStr,
    Submit,
    cltCode,
  }) => {
    const existingCoupons = await db.Coupon.findAll({ where: { cltCode } })
    return existingCoupons.map(coupon => ({
      number: coupon.number,
      date: moment(coupon.createdAt).format('YYYY.MM.DD')
    })
    )
  },
  getDeposit: async ({
    apiLogin,
    apiPassword,
    mode,
    busDtStr,
    Submit,
  }) => {
    const { mvVos: deposits } = await hantec.getDeposit({
      apiLogin,
      apiPassword,
      mode,
      busDtStr,
      Submit,
    })

    // deposits.push({
    //   coCode: 'HNZ',
    //   dt: '2019/10/07 00:00:00',
    //   cltCode: '52042',
    //   aeCode: '4010',
    //   ccy: 'USD',
    //   amt: 11000
    // })
    // deposits.push({
    //   coCode: 'HNZ',
    //   dt: '2019/10/07 00:00:00',
    //   cltCode: '52042',
    //   aeCode: '4010',
    //   ccy: 'USD',
    //   amt: 11000
    // })

    // insert deposits to db
    await Promise.all(deposits.map(async deposit => {
      const { aeCode, amt, ccy, cltCode, coCode, dt } = deposit
      const id = [aeCode, amt, ccy, cltCode, coCode, dt].join('|')
      await db.Deposit.findOrCreate({
        where: { id },
        defaults: {
          id,
          aeCode,
          amt,
          ccy,
          cltCode,
          coCode,
          dt,
        }
      })
    }))
    console.log(`incoming ${deposits.length} deposits`);


    // deposit to coupon logic
    const coupons = depositsToCoupons(deposits)

    // insert coupon to db
    const existingCoupons = await db.Coupon.findAll({
      group: ['cltCode'],
      attributes: ['cltCode', [Sequelize.fn('COUNT', 'cltCode'), 'count']],
    }).then(coupons => _
      .chain(coupons)
      .map(o => o.toJSON())
      .groupBy('cltCode')
      .mapValues(o => o[0].count)
      .value()
    )

    await async.eachSeries(coupons, async coupon => {
      if (!existingCoupons[coupon.cltCode]) {
        existingCoupons[coupon.cltCode] = 0
      }
      if (existingCoupons[coupon.cltCode] >= MAX_COUPON_PER_USER) {
        console.log('too many coupon for cltCode', coupon.cltCode);
        return
      }

      const [c, created] = await db.Coupon.findOrCreate({
        where: { id: coupon.id },
        defaults: {
          id: coupon.id,
          depositId: coupon.depositId,
          serialNumber: coupon.serialNumber,
          cltCode: coupon.cltCode,
          fromDeposit: coupon.fromDeposit,
          number: coupon.number,
        }
      })

      if (created) {
        console.log('coupon added for cltCode', coupon.cltCode);
        existingCoupons[coupon.cltCode] += 1
      }
    })
  }
}