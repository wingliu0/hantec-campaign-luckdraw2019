const { validationResult } = require('express-validator');

const validationErrorMiddleware = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    console.error('validationError', req.originalUrl, errors.array());
    res.status(422).jsonp(errors.array());
  } else {
    next();
  }
};

module.exports = {
  validationErrorMiddleware,
};
