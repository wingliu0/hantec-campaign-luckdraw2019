
const uuidv4 = require('uuid/v4');

module.exports = (sequelize, DataTypes) => {
  const Coupon = sequelize.define('Coupon', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: () => uuidv4().replace(/-/g, ''),
    },
    depositId: DataTypes.STRING,
    serialNumber: DataTypes.STRING,
    cltCode: DataTypes.STRING,
    fromDeposit: DataTypes.BOOLEAN,
    number: DataTypes.STRING,
    userId: DataTypes.STRING,
  }, {});
  Coupon.associate = function associate(models) {
    // associations can be defined here
  };
  return Coupon;
};
