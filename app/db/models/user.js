
const uuidv4 = require('uuid/v4');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: () => uuidv4().replace(/-/g, ''),
    },
    name: DataTypes.STRING,
    password: DataTypes.STRING,
    token: DataTypes.STRING,
  }, {});
  User.associate = function associate(models) {
    // associations can be defined here
  };
  return User;
};
