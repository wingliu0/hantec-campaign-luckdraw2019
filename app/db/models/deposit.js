
const uuidv4 = require('uuid/v4');

module.exports = (sequelize, DataTypes) => {
  const Deposit = sequelize.define('Deposit', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: () => uuidv4().replace(/-/g, ''),
    },
    coCode: DataTypes.STRING,
    dt: DataTypes.STRING,
    cltCode: DataTypes.STRING,
    aeCode: DataTypes.STRING,
    ccy: DataTypes.STRING,
    amt: DataTypes.DECIMAL(10, 2),
  }, {});
  Deposit.associate = function associate(models) {
    // associations can be defined here
  };
  return Deposit;
};
