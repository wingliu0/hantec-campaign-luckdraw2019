
const uuidv4 = require('uuid/v4');

module.exports = (sequelize, DataTypes) => {
  const Client = sequelize.define('Client', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: () => uuidv4().replace(/-/g, ''),
    },
    name: DataTypes.STRING,
    cltCode: DataTypes.STRING,
    token: DataTypes.STRING,
  }, {});
  Client.associate = function associate(models) {
    // associations can be defined here
  };
  return Client;
};
