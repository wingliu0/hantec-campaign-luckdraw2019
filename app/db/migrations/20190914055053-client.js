

module.exports = {
  up: (queryInterface, Sequelize) => Promise.all([
    queryInterface.createTable('Clients', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      name: Sequelize.STRING,
      cltCode: Sequelize.STRING,
      token: Sequelize.STRING,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    }),
  ]),

  down: (queryInterface, Sequelize) => Promise.all([
    queryInterface.dropTable('Clients'),
  ]),
};