

module.exports = {
  up: (queryInterface, Sequelize) => Promise.all([
    queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      name: Sequelize.STRING,
      password: Sequelize.STRING,
      token: Sequelize.STRING,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    }),
  ]),

  down: (queryInterface, Sequelize) => Promise.all([
    queryInterface.dropTable('Users'),
  ]),
};