

module.exports = {
  up: (queryInterface, Sequelize) => Promise.all([
    queryInterface.createTable('Coupons', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      depositId: Sequelize.STRING,
      serialNumber: Sequelize.STRING,
      cltCode: Sequelize.STRING,
      fromDeposit: Sequelize.BOOLEAN,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    }),
    queryInterface.createTable('Deposits', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      coCode: Sequelize.STRING,
      dt: Sequelize.STRING,
      cltCode: Sequelize.STRING,
      aeCode: Sequelize.STRING,
      ccy: Sequelize.STRING,
      amt: Sequelize.DECIMAL(10, 2),
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    }),
  ]),

  down: (queryInterface, Sequelize) => Promise.all([
    queryInterface.dropTable('Coupons'),
    queryInterface.dropTable('Deposits'),
  ]),
};