

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('Coupons', 'userId', Sequelize.STRING),

  down: (queryInterface, Sequelize) => queryInterface.removeColumn('Coupons', 'userId'),
};
