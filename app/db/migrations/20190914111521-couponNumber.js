

module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.addColumn('Coupons', 'number', Sequelize.STRING),

  down: (queryInterface, Sequelize) => queryInterface.removeColumn('Coupons', 'number'),
};
