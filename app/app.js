const path = require('path');
const express = require('express');
const uuidv4 = require('uuid/v4');
const _ = require('lodash');
const cors = require('cors');
const bodyParser = require('body-parser');
const axios = require('axios');
const moment = require('moment');
const db = require('./db/models');
const api = require('./api');
const { validationErrorMiddleware } = require('./libs/express-validator');
const { check, header } = require('express-validator');
const { matchedData } = require('express-validator');
const { randomNumber } = require('./libs/utility')
const scheduler = require('./schedulers')
scheduler.start()

const MAX_COUPON_PER_USER = 10
const generateCoupons = (
  number,
  cltCode,
  userId,
) => {
  const numberOfCoupons = Number(number)
  if (!numberOfCoupons) { return [] }
  const baseId = uuidv4().replace(/-/g, '')
  return Array.from(new Array(numberOfCoupons), (v, i) => {
    const serialNumber = `${i + 1}/${numberOfCoupons}`
    const number = randomNumber(6)
    return {
      id: `${baseId}|${serialNumber}`,
      serialNumber,
      userId,
      cltCode,
      fromDeposit: false,
      number,
    }
  })
}

const app = express();

app.use('/campaign/luckdraw2019', express.static(__dirname + '/public/web'))
app.use('/campaign/luckdraw2019/admin', express.static(__dirname + '/public/admin/dist'))
app.get('/campaign/luckdraw2019/admin/*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'public/admin/dist/index.html'))
})
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
function clientErrorHandler(err, req, res, next) {
  if (typeof _.get(err, 'toString') === 'function') {
    res.status(500).send({ error: err.toString() || 'Something failed!' });
  } else {
    res.status(500).send({ error: 'Something failed!' });
  }
}
app.use(clientErrorHandler)
app.get('/campaign/luckdraw2019/api/clientGetCoupon', async (req, res) => {
  try {
    const { token } = req.query
    const client = await db.Client.findOne({ where: { token } })
    const coupons = await api.getCoupons({
      cltCode: client.cltCode,
    }).then(r => r.sort())
    res.send({ coupons, name: client.name })

  } catch (error) {
    res.status(500).send(error)
  }
})
app.post('/campaign/luckdraw2019/api/clientLogin', async (req, res) => {
  try {
    var { name, password } = req.body
    if (!name || !password) { throw '!' }
    const { token } = await api.checkLogin({
      cltCode: name,
      loginPwd: password,
    }).catch(e => {
      // console.error(e);
      throw e
    })
    if (token) {
      res.send({ token })
    } else {
      throw 'login error'
    }
  } catch (error) {
    res.status(500).send(error)
  }
})
app.post('/campaign/luckdraw2019/api/login', [
  check('name', 'invalid').isString().not().isEmpty(),
  check('password', 'invalid').isString().not().isEmpty(),
], validationErrorMiddleware, async (req, res) => {
  try {
    const { name, password } = matchedData(req);
    const user = await db.User.findOne({
      where: {
        name,
        password,
      }
    })
    if (!user) { throw 'user not found' }
    // const token = uuidv4().replace(/-/g, '') + `-${moment().format('YYYYMMDD')}`
    // await user.update({ token })
    res.send({ token: user.token, name: user.name })
  } catch (error) {
    console.error(error);
    res.status(500).send(error)
  }
})

// admin api
app.get('/campaign/luckdraw2019/api/admin/coupon', async (req, res) => {
  try {
    const token = req.headers['x-token']
    if (!token) { throw '!token' }
    const user = await db.User.findOne({ where: { token } })
    if (!user) { throw '!user' }
    const coupons = await db.Coupon.findAll()
      .then(coupons => coupons.map(c => c.toJSON()))
      .then(coupons => _.orderBy(coupons.map(c => _.pick(c, ['id', 'number', 'cltCode', 'fromDeposit', 'userId', 'createdAt'])), ['createdAt'], ['desc']))
    res.send({ coupons })
  } catch (error) {
    console.error(error);
    res.status(500).send(error)
  }
})
app.post('/campaign/luckdraw2019/api/admin/coupon', [
  header('x-token', 'invalid').exists(),
  check('number', 'invalid').not().isEmpty(),
  check('clientCode', 'invalid').not().isEmpty(),
], validationErrorMiddleware, async (req, res) => {
  try {
    const data = matchedData(req);
    const { clientCode } = data
    const number = Number(data.number)
    const token = data['x-token']
    if (!token) { throw '!token' }
    const user = await db.User.findOne({ where: { token } })
    if (!user) { throw '!user' }
    const existingCoupons = await db.Coupon.findAll({ where: { cltCode: clientCode } })
    console.log({ existingCoupons }, number);

    if ((existingCoupons.length + number) > MAX_COUPON_PER_USER) {
      throw `too many coupon, Max is ${MAX_COUPON_PER_USER - existingCoupons.length}`
    }
    const newCoupons = generateCoupons(Math.min(MAX_COUPON_PER_USER - existingCoupons.length, number), clientCode, user.id)
    // insert coupon to db
    await Promise.all(newCoupons.map(async coupon => {
      await db.Coupon.findOrCreate({
        where: { id: coupon.id },
        defaults: {
          id: coupon.id,
          serialNumber: coupon.serialNumber,
          userId: coupon.userId,
          cltCode: coupon.cltCode,
          fromDeposit: coupon.fromDeposit,
          number: coupon.number,
        }
      })
    }))

    // get and return all coupon
    const coupons = await db.Coupon.findAll()
      .then(coupons => coupons.map(c => c.toJSON()))
      .then(coupons => _.orderBy(coupons.map(c => _.pick(c, ['id', 'number', 'cltCode', 'fromDeposit', 'userId', 'createdAt'])), ['createdAt'], ['desc']))
    res.send({ coupons })

  } catch (error) {
    console.error(error)
    res.status(500).send(error)
  }
})
app.delete('/campaign/luckdraw2019/api/admin/coupon', [
  header('x-token', 'invalid').exists(),
  check('x-id', 'invalid').not().isEmpty(),
], validationErrorMiddleware, async (req, res) => {
  try {
    const { 'x-token': token, 'x-id': couponId } = matchedData(req);
    const user = await db.User.findOne({ where: { token } })
    if (!user) { throw '!user' }
    await db.Coupon.destroy({ where: { id: couponId } })
    // get and return all coupon
    const coupons = await db.Coupon.findAll()
      .then(coupons => coupons.map(c => c.toJSON()))
      .then(coupons => _.orderBy(coupons.map(c => _.pick(c, ['id', 'number', 'cltCode', 'fromDeposit', 'userId', 'createdAt'])), ['createdAt'], ['desc']))
    res.send({ coupons })
  } catch (error) {
    res.status(500).send(error)
  }
})

const PORT = 3859
app.listen(PORT, function () {
  console.log(`htcampaign app listening on port ${PORT}!`);
});
